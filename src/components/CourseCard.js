import { useState, useEffect } from 'react';
import { Button, Card } from 'react-bootstrap';

// Destructuring is done in the parameter to retrieve the courseProp.
export default function CourseCard({courseProp}){

	// console.log(props.courseProp);
	// console.log(courseProp);

	const {name, description, price} = courseProp;


	// Use the state hook for this component to be able to store its state.
	// States are used to keep track of information related to individual components
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue)


	// When a component mounts (loads for the first time), any associated states will undergo a state change from null to the given initial/default state

	// e.g. count below goes from null to "0", since 0 is our initial state


	const [count, setCount] = useState(0);

	// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.
	// console.log(useState(0));


	// Function that keeps track of the enrollees for a course
	// By default Javascript is synchronous, as it executes code from the top of the file all the wa to the bottom and will wait for the completion of one expression before it proceeds to the next

	// The setter function for useStates are asynchronous, allowing it to execute seperately from other codes in the program.

	// The "setCount" function is being executed while the "console.log" is already being completed resulting in the console to be behind by one count.

	// si getter naman is to view yung value nung use state po natin, while si setter natin is to change po yung initial value sa may loob nung useState() natin. si onClick (and other event handler) for reactJS sya, kasi we are using JSX format. Bale ganyan talaga format nya

	const [seats, setSeat] = useState(30);

	function enroll () {
		// setCount(count + 1);
		// console.log('Enrollees: ' + count)

		// if (count < 30) {
			setCount(count + 1);
			setSeat(seats - 1);
			// console.log('Enrollees: ' + count)
		// } else {
		// 	setCount(count)
		// 	alert (`No more seats`)
		// }
	}

	// useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on in initial page load)

	// if the array is blank, the code will be executed on component mount ONLY

	// Do NOT omit the array completely
	useEffect(() => {
		console.log("State changed")
	}, [count,seats])


	/*
		syntax: 
		useEffect(() => {
			code to be executed
		}, [state(s) to monitor])


	*/
	return(
			<Card className="mb-2">
				<Card.Body>
				  <Card.Title>{name}</Card.Title>
				  <Card.Subtitle>Description:</Card.Subtitle>
				  <Card.Text>{description}</Card.Text>
				  <Card.Subtitle>Price:</Card.Subtitle>
				  <Card.Text>Php {price}</Card.Text>
				  <Card.Text>Enrollees: {count}</Card.Text>
				  <Card.Text>Seats: {seats}</Card.Text>
				   <Button variant ="primary" onClick={enroll}>Enroll</Button>
				</Card.Body>
			 </Card>
		
	)
}

