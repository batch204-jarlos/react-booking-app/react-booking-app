import { useState, useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import  UserContext  from '../UserContext';
import { Redirect } from 'react-router-dom';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	// const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	const { user, setUser } = useContext(UserContext);

	

	/*
	To properly change and save input valuess, we must implement two -way binding
	We need to capture whatever the user types in the input as they are typing
	Meaning we need the input's .value value
	To get the value, we capture then event (in this case, onChange). The target of the onChange event is the input,meaning we can get the .value

	*/

	useEffect(() => {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)
		if((email !== '' && password1 !== '' )) {
			setIsActive(true)
			}else{
			setIsActive(false)
			
		}
	}, [email, password1])

	function loginUser(e){
		e.preventDefault()

		// localStorage.setItem allows us to save a key/value pair to localStorage
		// Syntax: (key,value)
		// localStorage.setItem('email', email)

		// Activity:
		/*
			Create a fetch request inside this function to allow users to log in. Log in the console the response.
		*/

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
		// setUser({
		// 	email: email
		// })


		// setEmail("")
		// setPassword1("")

		
		// // setPassword2("")
		// alert('Thank you for logging in!');

		// console.log(localStorage.getItem("email"))
	}

	// function authenticateUser(e){
	// 	e.preventDefault()

		

	// 	setEmail("")
	// 	setPassword("")

	// 	alert('Thank you for logging in');
	// }

	return(
		
		(localStorage.getItem("email") !== null) ?
		  <Redirect to="/" />
		  :
		  <>
		    <Form onSubmit ={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value ={email}
					onChange ={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					value ={password1}
					onChange ={e => setPassword1(e.target.value)}
					required
				/>
				
			</Form.Group>

			{/*<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value ={password2}
					onChange ={e => setPassword2(e.target.value)}
					required
				/>
				
			</Form.Group>*/}

			{isActive ?
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<Button className="mt-3" variant="primary"id="submitBtn" disabled>
				Submit
			</Button>


			}

			
		</Form>
		  </>

		

		
		

		
		)
}


