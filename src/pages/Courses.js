import { useEffect, useState } from 'react';
import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	
		const [coursesData, setCoursesData] = useState([]);
		// Check to see if the mock data was captured
		// console.log(courseData);
		// console.log(courseData[0]);

		// Props
			// is a shorthand for "property" since components are considered as object in ReactJS.
			// Props is a way to pass data from a parent component to a child component.
			// It is a synonymous to the function parameter.

		// Fetch by default always a GET request, unless a different one is specified
		
		// ALWAYS add fetch requests for getting data in a useEffect hook

		useEffect(() => {


			fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
			.then(res => res.json())
			.then(data => {
				// console.log(data)
				setCoursesData(data)
			})

		}, [])


		const courses = coursesData.map(course => {
			return(
				<CourseCard courseProp={course} key={course._id} />
				)
		})

		return (
			<>
				<h1>Courses</h1>
				{courses}
				
			</>
	)
}

// map is to access each element ni courseData then pass it to coursecard po